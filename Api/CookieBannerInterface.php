<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Api;

interface CookieBannerInterface
{
    /**
     * @return \Envisage\CookieBanner\Api\CookieBannerInterface[]
     */
    public function getCookieBannerData(): array;
}
