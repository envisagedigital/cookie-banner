requirejs([
    'jquery',
    'mage/cookies'
], function ($) {
    'use strict';

    $(document).ready(function () {
        
        let isCrawler = false;
        let cookieLifetime = false;

        $.when(getCookieBannerData().done(function (data) {
            if (data[0]) {
                isCrawler = data[0]['is_crawler'];
                cookieLifetime = data[0]['cookie_lifetime'];
                let cookieCompliance = getCookie();
                if (!cookieCompliance && !isCrawler && cookieLifetime) {
                    $('div.cookie-banner').show();
                }
            }
        }));

        $('.hide-banner').on('click', { cookieLifetime: cookieLifetime }, setCookie);
        
    });

    function getCookieBannerData() {
        return $.getJSON('/rest/all/V1/cookie-compliance/data');
    };

    function getCookie() {
        return $.cookie('envisage_cookiecompliance');
    }

    function setCookie(event) {
        $('.cookie-banner').hide();
        $.cookie('envisage_cookiecompliance', true, { expires: event.data.cookieLifetime, path: '/', secure: true });
    };
});