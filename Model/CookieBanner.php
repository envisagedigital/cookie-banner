<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Model;

class CookieBanner implements \Envisage\CookieBanner\Api\CookieBannerInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Envisage\CookieBanner\Model\ConfigInterface
     */
    private $config;

    /**
     * @var \Envisage\CookieBanner\Model\WebCrawlerInterface
     */
    private $webCrawler;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Envisage\CookieBanner\Model\ConfigInterface $config
     * @param \Envisage\CookieBanner\Model\WebCrawlerInterface $webCrawler
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Envisage\CookieBanner\Model\ConfigInterface $config,
        \Envisage\CookieBanner\Model\WebCrawlerInterface $webCrawler
    ) {
        $this->logger = $logger;
        $this->config = $config;
        $this->webCrawler = $webCrawler;
    }

    /**
     * @inheritdoc
     */
    public function getCookieBannerData(): array
    {
        $ret = [];
        try {
            if ($this->config->getEnabled()) {
                $ret[] = $this->parseCookieBannerData();
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $ret;
    }

    /**
     * @return array
     */
    private function parseCookieBannerData(): array
    {
        return [
            'cookie_lifetime' => $this->config->getCookieLifetime(),
            'is_crawler' => $this->webCrawler->isWebCrawler()
        ];
    }
}
