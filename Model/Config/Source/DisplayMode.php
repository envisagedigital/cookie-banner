<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Model\Config\Source;

class DisplayMode implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 0, 'label' => __('Top of page')],
            ['value' => 1, 'label' => __('Pop up')],
            ['value' => 2, 'label' => __('Bottom of page')],
        ];
    }
}
