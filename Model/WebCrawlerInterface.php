<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Model;

interface WebCrawlerInterface
{
    /**
     * @return bool
     */
    public function isWebCrawler(): bool;
}
