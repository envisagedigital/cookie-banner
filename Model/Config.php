<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Model;

use Magento\Store\Model\ScopeInterface;

class Config implements ConfigInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritdoc
     */
    public function getEnabled(): string
    {
        return $this->getConfigData(self::XML_PATH_ENABLED);
    }

    /**
     * @inheritdoc
     */
    public function getMessage(): string
    {
        return $this->getConfigData(self::XML_PATH_MESSAGE);
    }

    /**
     * @inheritdoc
     */
    public function getLinkSeparator(): string
    {
        return $this->getConfigData(self::XML_PATH_LINK_SEPARATOR);
    }

    /**
     * @inheritdoc
     */
    public function getLearnLink(): string
    {
        return $this->getConfigData(self::XML_PATH_LEARN_LINK);
    }

    /**
     * @inheritdoc
     */
    public function getLearnMessage(): string
    {
        return $this->getConfigData(self::XML_PATH_LEARN_MESSAGE);
    }

    /**
     * @inheritdoc
     */
    public function getHideMessage(): string
    {
        return $this->getConfigData(self::XML_PATH_HIDE_MESSAGE);
    }

    /**
     * @inheritdoc
     */
    public function getCookieLifetime(): string
    {
        return $this->getConfigData(self::XML_PATH_COOKIE_LIFETIME);
    }

    /**
     * @inheritdoc
     */
    public function getCustomCss(): string
    {
        return $this->getConfigData(self::XML_PATH_CUSTOM_CSS);
    }

    /**
     * @inheritdoc
     */
    public function getBackgroundColour(): string
    {
        return $this->getConfigData(self::XML_PATH_BACKGROUND_COLOUR);
    }

    /**
     * @inheritdoc
     */
    public function getTextColour(): string
    {
        return $this->getConfigData(self::XML_PATH_TEXT_COLOUR);
    }

    /**
     * @inheritdoc
     */
    public function getLinkColour(): string
    {
        return $this->getConfigData(self::XML_PATH_LINK_COLOUR);
    }

    /**
     * @inheritdoc
     */
    public function getCrawlerList(): string
    {
        return $this->getConfigData(self::XML_PATH_CRAWLER_LIST);
    }

    /**
     * @inheritdoc
     */
    public function getDisplayMode(): string
    {
        return $this->getConfigData(self::XML_PATH_DISPALY_MODE);
    }

    /**
     * @param string $path
     * @return string
     */
    private function getConfigData(string $path): string
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE) ?? '';
    }
}
