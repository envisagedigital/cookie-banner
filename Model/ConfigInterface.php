<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Model;

interface ConfigInterface
{
    const XML_PATH_ENABLED = 'envisage_cookiebanner/general/enabled';
    const XML_PATH_MESSAGE = 'envisage_cookiebanner/general/message';
    const XML_PATH_LINK_SEPARATOR = 'envisage_cookiebanner/general/link_separator';
    const XML_PATH_LEARN_LINK = 'envisage_cookiebanner/general/learn_link';
    const XML_PATH_LEARN_MESSAGE = 'envisage_cookiebanner/general/learn_message';
    const XML_PATH_HIDE_MESSAGE = 'envisage_cookiebanner/general/hide_message';
    const XML_PATH_COOKIE_LIFETIME = 'envisage_cookiebanner/general/cookie_lifetime';
    const XML_PATH_CUSTOM_CSS = 'envisage_cookiebanner/display/custom_css';
    const XML_PATH_BACKGROUND_COLOUR = 'envisage_cookiebanner/display/background_colour';
    const XML_PATH_TEXT_COLOUR = 'envisage_cookiebanner/display/text_colour';
    const XML_PATH_LINK_COLOUR = 'envisage_cookiebanner/display/link_colour';
    const XML_PATH_CRAWLER_LIST = 'envisage_cookiebanner/crawlers/crawler_list';
    const XML_PATH_DISPALY_MODE = 'envisage_cookiebanner/display/display_mode';

    /**
     * @return string
     */
    public function getEnabled(): string;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @return string
     */
    public function getLinkSeparator(): string;

    /**
     * @return string
     */
    public function getLearnLink(): string;

    /**
     * @return string
     */
    public function getLearnMessage(): string;

    /**
     * @return string
     */
    public function getHideMessage(): string;

    /**
     * @return string
     */
    public function getCookieLifetime(): string;

    /**
     * @return string
     */
    public function getCustomCss(): string;

    /**
     * @return string
     */
    public function getBackgroundColour(): string;

    /**
     * @return string
     */
    public function getTextColour(): string;

    /**
     * @return string
     */
    public function getLinkColour(): string;

    /**
     * @return string
     */
    public function getCrawlerList(): string;

    /**
     * @return string
     */
    public function getDisplayMode(): string;
}
