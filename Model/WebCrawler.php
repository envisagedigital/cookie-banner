<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\Model;

class WebCrawler implements WebCrawlerInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Envisage\CookieBanner\Model\ConfigInterface
     */
    private $config;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Envisage\CookieBanner\Model\ConfigInterface $config
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\RequestInterface $request,
        \Envisage\CookieBanner\Model\ConfigInterface $config
    ) {
        $this->logger = $logger;
        $this->request = $request;
        $this->config = $config;
    }

    public function isWebCrawler(): bool
    {
        $ret = false;
        try {
            if ($this->parseCrawlerList() && $this->getUserAgent()) {
                $ret = $this->crawlerSearch();
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return (bool) $ret;
    }

    /**
     * @return array
     */
    private function parseCrawlerList(): array
    {
        return \explode("|", $this->config->getCrawlerList());
    }

    /**
     * @return string
     */
    private function getUserAgent(): string
    {
        return \strtolower($this->request->getServer('HTTP_USER_AGENT')) ?? '';
    }

    /**
     * @return bool
     */
    private function crawlerSearch(): bool
    {
        foreach ($this->parseCrawlerList() as $value) {
            if (\strpos($this->getUserAgent(), \strtolower($value)) !== false)
                return true;
        }

        return false;
    }
}
