<?php

declare(strict_types=1);

namespace Envisage\CookieBanner\ViewModel;

class CookieBanner implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    const DISPLAY_MODE_TOP = 'top-page-cookie-banner';
    const DISPLAY_MODE_BOTTOM = 'bottom-page-cookie-banner';
    const DISPLAY_MODE_POPUP = 'pop-up-cookie-banner';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Envisage\CookieBanner\Model\ConfigInterface
     */
    private $config;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Envisage\CookieBanner\Model\ConfigInterface $config
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Envisage\CookieBanner\Model\ConfigInterface $config
    ) {
        $this->logger = $logger;
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getCookieBannerData(): array
    {
        $ret = [];
        try {
            if ($this->config->getEnabled()) {
                $ret = $this->parseCookieBannerData();
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $ret;
    }

    /**
     * @return array
     */
    private function parseCookieBannerData(): array
    {
        return [
            'display_mode' => $this->getDisplayMode(),
            'message' => $this->config->getMessage(),
            'link_separator' => $this->config->getLinkSeparator(),
            'learn_link' => $this->config->getLearnLink(),
            'learn_message' => $this->config->getLearnMessage(),
            'hide_message' => $this->config->getHideMessage(),
            'custom_css' => $this->config->getCustomCss(),
            'background_colour' => $this->config->getBackgroundColour(),
            'text_colour' => $this->config->getTextColour(),
            'link_colour' => $this->config->getLinkColour()
        ];
    }

    /**
     * @return string
     */
    private function getDisplayMode(): string
    {
        $ret = self::DISPLAY_MODE_TOP;
        switch ($this->config->getDisplayMode()) {
            case 1:
                $ret = self::DISPLAY_MODE_POPUP;
                break;
            case 2:
                $ret = self::DISPLAY_MODE_BOTTOM;
                break;
        }

        return $ret;
    }
}
